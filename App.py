from flask import Flask


zorro = Flask(__name__)

@zorro.route('/')
def index():
    return ("Welcome zorro")

@zorro.route('/add/<x>/<y>',methods=['GET','POST'])
def add(x,y):
    return str(int(x)+int(y))

@zorro.route('/home')
def say():
    return ("Welcome home")

@zorro.route('/about')
def add_about():
    return ("Welcome to About")

@zorro.route('/contact')
def add_contact():
    return ("Here is your contacts list")

@zorro.route('/links')
def add_link():
    return ("This is your link")

@zorro.route('/calc/<w>/<z>',methods=['GET','POST'])
def calc(w,z):
    return str(int(w)*int(z))


zorro.run()



